# viro

Loads your environment so you don't have to.

Offers a data structure for serializing and deserializing environment variables, to allow for easy
loading of `/etc/profile` scripts in non-POSIX shells like `fish`. The `viro` binary is only
available for unix-based systems at the moment; see the "Roadmap" section of this README for
details.

## License

Available via the [Anti-Capitalist Software License][ACSL] for individuals, non-profit
organisations, and worker-owned businesses.

[ACSL]: ./LICENSE.md

## Installation

First, build `viro` for your system and install it in `/usr/bin`, alongside the `viro-profile` script.

Depending on whether you want to use systemd or no d, merge the `/usr` provided in either
`systemd/usr` or `nod/usr` with your own. If you use Arch Linux, a version of the `systemd` files
is offered as a `PKGBUILD` in the `pkg` directory.

## Usage

Because bash does not have a standardized config directory, you will need to source the scripts in
`/usr/share/bash/bashrc.d` in order to make this work. If you chose to use systemd, you must also
enable the `profile.service` for your user.

## Roadmap

The `viro` crate has been fully ported to using `OsString`s instead of byte sequences, meaning
that it in theory should be portable to Windows. However, after doing this, I decided that I'm
not going to simp for Microsoft, and did not actually implement a Windows version.

The main issue is that it should be outputting "wide character" strings in all cases on Windows,
but Rust specifically takes steps to ensure that `stdio` transforms UTF-8 output into what
Windows expects, and doesn't actually convert all [WTF-8] `OsString`s into wide characters.
So, special code would have to be written using the Windows API to properly account for this.

Additionally, there are currently no attempts to escape `\x3D` (equals sign) and `\x00` (NUL) bytes
when outputting variables, and this simply alters the output. A future version of this crate will
want to offer these, but I have no plans to implement this at the moment.

[WTF-8]: https://simonsapin.github.io/wtf-8
