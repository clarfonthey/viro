#!/bin/sh
unset __load_profile
viro-profile | xargs -t0 systemctl --user set-environment
exec systemd-notify --ready
