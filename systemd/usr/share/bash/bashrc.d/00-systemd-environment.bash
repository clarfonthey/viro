case "$-" in
    *i*) ;;
    *) if [[ -z ${__viro_loaded+x} ]]; then
        systemctl --user start profile
        systemctl --user show-environment | while read -r var; do
            export "${var?}"
        done
        export __viro_loaded=1
    fi
esac
