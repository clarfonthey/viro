if status --is-login
    if not set -q __viro_loaded
        systemctl --user start profile
        for var in (systemctl --user show-environment | string replace --regex '\$\'(.*)\'' '$1')
            set -gx (string split -m 1 = $var)
        end
        set -gx __viro_loaded 1
    end
end
