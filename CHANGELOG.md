This project uses a **major**.**minor**.**micro** versioning scheme, where:

* Bumping the major version resets the minor version to zero.
* Bumping the minor version resets the micro version to zero.
* The major version is bumped if the output format or behaviour of the `viro` binary changes, or
  there are breaking changes to the `viro` crate as defined by Rust RFC 1122.
* The minor version is bumped on minor changes to the `viro` crate, as defined by Rust RFC 1122.
* The micro version is bumped in all other cases.

# v2.0.0

This release completely refactors the crate to work with `OsStr`, which paves
the path to true Windows support. However, upon realising that I should not simp
Microsoft, Windows support has not been added, even though it is now
theoretically possible.

To ensure this works as smoothly as possible, generic reader/writer support is
no longer allowed under the assumption that things would be serialized directly
to wide characters on Windows instead of through WTF-8.

Additionally, async support has been removed considering how stdin/stdout are
generally always synchronous, and it reduces the dependencies required.

* [changed] API has been rewritten entirely to use `OsStr` instead of `BStr`
* [changed] Updated to 2024 edition
* [fixed] Now works on stable Rust, with an optional `nightly` feature
* [removed] Async methods and generic reader/writer methods

# v1.0.4

* [changed] Updated to 2021 edition
* [changed] Updated URL to repository in Cargo.toml

# v1.0.3

* [changed] Updated URL to repository in Cargo.toml

# v1.0.2

* [fixed] Now builds without `sync` and `async` crate features

# v1.0.1 (yanked)

* [fixed] No longer depends on unstable `async_closure` feature

# v1.0.0 (yanked)

This is the first release.
