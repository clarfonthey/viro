#!/bin/sh
case "$-" in
    *i*) ;;
    *) if [[ -z ${__viro_loaded+x} ]]; then
        viro-profile | IFS='\0' while read -r var; do
            export "${var?}"
        done
        export __viro_loaded=1
    fi
esac
