#!/bin/sh
if status --is-login
    if not set -q __viro_loaded
        for var in (viro-profile | string split0)
            export "${var?}"
        end
        set -gx __viro_loaded 1
    end
end
