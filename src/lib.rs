//! Loads your environment so you don't have to.
//!
//! This is intended as a portable way to share environments between processes, particularly for
//! loading scripts in `/etc/profile.d`.
//!
//! This crate offers a simple way of serializing an environment and a binary that can both read
//! environment variables from stdin and combine them into stdout.
//!
//! See the [`Viro`] type for more details.
#![cfg_attr(doc, feature(doc_auto_cfg))]
#![cfg_attr(feature = "nightly", feature(coverage_attribute))]

#[cfg(windows)]
compile_error!("Windows is not yet supported.");

use std::{
    env,
    ffi::{OsStr, OsString},
    io::{self, BufRead as _, BufReader, BufWriter, Write as _, stdin, stdout},
};

use self::storage::Storage;
pub use self::storage::{
    Environ, EnvironParts, Scalar, ScalarParts, Var, VarParts, Vector, VectorParts,
};

mod storage;

/// An environment.
///
/// This uses a hard-coded set of environment variables to determine whether a variable should be a
/// "scalar", having a singular value, or a "vector", having multiple values. It also checks what
/// separators are used for particular variables, and excludes certain variables which are not
/// intended to be passed around.
///
/// For example, `PATH` is a vector which separates its values using semicolons on Windows and
/// colons on other platforms; and the `SHELL` variable is one that should not be included.
///
/// The currently known variables are:
///
/// Key                    | Behavior
/// -----------------------|----------------------------------------------------------------
/// `_`                    | Excluded
/// `CAML_LD_LIBRARY_PATH` | Separated by colons; may also be separated by semicolons
/// `GOPATH`               | Separated by colons
/// `GPG_TTY`              | Excluded
/// `HOME`                 | Excluded
/// `LD_LIBRARY_PATH`      | Separated by colons; may also be separated by semicolons
/// `LD_PRELOAD`           | Separated by colons; may also be separated by spaces
/// `LS_COLORS`            | Terminated by colons
/// `MANPATH`              | Separated by colons
/// `NOTIFY_SOCKET`        | Excluded
/// `PATH`                 | Separated by semicolons on windows, colons on other platforms
/// `PERL5LIB`             | Separated by colons
/// `PERLLIB`              | Separated by colons
/// `PKG_CONFIG_PATH`      | Separated by semicolons on windows, colons on other platforms
/// `PWD`                  | Excluded
/// `PYTHONPATH`           | Separated by colons
/// `SHELL`                | Excluded
/// `SHLVL`                | Excluded
/// `USER`                 | Excluded
/// `XDG_DATA_DIRS`        | Separated by colons
#[derive(Default, Clone, Debug)]
pub struct Viro {
    /// Inner storage.
    storage: Storage,
}
impl Viro {
    /// Makes an empty environment.
    ///
    /// # Examples
    ///
    /// ```
    /// use viro::Viro;
    ///
    /// let mut env = Viro::new();
    ///
    /// let mut ser = env.write_string();
    /// assert_eq!(ser, "");
    /// ```
    #[cfg_attr(feature = "nightly", coverage(off))]
    pub fn new() -> Viro {
        Viro {
            storage: Storage::new(),
        }
    }

    /// Inserts a key-value pair into the environment.
    ///
    /// Note that this does not verify the pair for validity: in particular, if the key contains an
    /// equals sign (U+3D) or either string contains a NUL value (U+00), then the written version
    /// of the environment will be incorrect.
    ///
    /// # Examples
    ///
    /// ```
    /// use viro::Viro;
    ///
    /// let mut env = Viro::new();
    /// env.insert("PATH", "/bin:/sbin");
    /// env.insert("PATH", "/sbin:/bin:/usr/bin");
    /// env.insert("SHELL", "/usr/bin/fish");
    /// env.insert("EDITOR", "/usr/bin/kak");
    ///
    /// let ser = env.write_string();
    /// assert_eq!(ser, "PATH=/bin:/sbin:/usr/bin\0EDITOR=/usr/bin/kak\0");
    /// ```
    #[cfg_attr(feature = "nightly", coverage(off))]
    pub fn insert<K: AsRef<OsStr>, V: AsRef<OsStr>>(&mut self, key: K, value: V) {
        self.storage
            .insert(key.as_ref().into(), value.as_ref().into())
    }

    /// Removes a key-value pair from the environment.
    ///
    /// # Examples
    ///
    /// ```
    /// use viro::Viro;
    ///
    /// let mut env = Viro::new();
    /// env.insert("PATH", "/bin:/sbin");
    /// env.remove("PATH");
    /// env.insert("PATH", "/sbin:/bin:/usr/bin");
    ///
    /// let ser = env.write_string();
    /// assert_eq!(ser, "PATH=/sbin:/bin:/usr/bin\0");
    /// ```
    #[cfg_attr(feature = "nightly", coverage(off))]
    pub fn remove<K: AsRef<OsStr>>(&mut self, key: K) {
        self.storage.remove(key.as_ref())
    }

    /// Processes a single variable from an OS string.
    ///
    /// This is equivalent to [`insert`] if the item contains an equals sign, and [`remove`] if the
    /// item does not contain an equal sign.
    ///
    /// [`insert`]: Self::insert
    /// [`remove`]: Self::remove
    ///
    /// Like [`insert`], this does not verify the pair for validity: in particular, if the key
    /// contains an equals sign (U+3D) or either string contains a NUL value (U+00), then the
    /// written version of the environment will be incorrect.
    ///
    /// # Examples
    ///
    /// ```
    /// use viro::Viro;
    ///
    /// let mut env = Viro::new();
    /// env.process("PATH=/bin:/sbin");
    /// env.process("PATH");
    /// env.process("PATH=/sbin:/bin:/usr/bin");
    ///
    /// let ser = env.write_string();
    /// assert_eq!(ser, "PATH=/sbin:/bin:/usr/bin\0");
    /// ```
    #[cfg_attr(feature = "nightly", coverage(off))]
    pub fn process<S: AsRef<OsStr>>(&mut self, item: S) {
        self.storage.process(item.as_ref().as_encoded_bytes())
    }

    /// Gets a variable from the environment.
    ///
    /// # Examples
    ///
    /// ```
    /// use std::ffi::OsString;
    /// use viro::Viro;
    ///
    /// let mut env = Viro::new();
    /// env.insert("PATH", "/bin:/sbin");
    /// assert_eq!(env.get("PATH").unwrap().into_parts().collect::<OsString>(), "PATH=/bin:/sbin");
    /// ```
    #[cfg_attr(feature = "nightly", coverage(off))]
    pub fn get<S: AsRef<OsStr>>(&mut self, key: S) -> Option<Var<'_>> {
        self.storage.get(key.as_ref())
    }

    /// Gets all the variables in the environment.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use std::ffi::OsString;
    /// use viro::Viro;
    ///
    /// let mut env = Viro::new();
    /// env.insert("SHELL", "/bin/bash");
    /// env.insert("PATH", "/sbin:/bin:/usr/bin");
    /// let mut env = env.environ();
    /// assert_eq!(env.next().unwrap().into_parts().collect::<OsString>(), "SHELL=/bin/bash");
    /// assert_eq!(env.next().unwrap().into_parts().collect::<OsString>(), "PATH=/sbin:/bin:/usr/bin");
    /// assert!(env.next().is_none());
    /// ```
    #[cfg_attr(feature = "nightly", coverage(off))]
    pub fn environ(&self) -> Environ<'_> {
        self.storage.environ()
    }

    /// Reads the environment variables directly.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use viro::Viro;
    ///
    /// let mut env = Viro::new();
    /// env.read_environ();
    /// ```
    #[cfg_attr(feature = "nightly", coverage(off))]
    pub fn read_environ(&mut self) {
        env::vars_os().for_each(|(key, value)| self.insert(key, value))
    }

    /// Processes the whole environment from an OS string.
    ///
    /// This calls [`process`](Self::process) for each item separated by a NUL value.
    ///
    /// # Examples
    ///
    /// ```
    /// use viro::Viro;
    ///
    /// let mut env = Viro::new();
    /// env.read_string("PATH=/bin:/sbin\0PATH\0PATH=/sbin:/bin:/usr/bin");
    ///
    /// let ser = env.write_string();
    /// assert_eq!(ser, "PATH=/sbin:/bin:/usr/bin\0");
    /// ```
    #[cfg_attr(feature = "nightly", coverage(off))]
    pub fn read_string<S: AsRef<OsStr>>(&mut self, data: S) {
        self.storage.process_all(data.as_ref().as_encoded_bytes())
    }

    /// Reads the environment from standard input.
    ///
    /// This has the same behavior as [`read_string`](Self::read_string), except taking the data
    /// from standard input.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// # fn main() -> std::io::Result<()> {
    /// use viro::Viro;
    ///
    /// let mut env = Viro::new();
    /// env.read_stdin()?;
    /// # Ok(())
    /// # }
    /// ```
    #[cfg_attr(feature = "nightly", coverage(off))]
    pub fn read_stdin(&mut self) -> io::Result<()> {
        let mut reader = BufReader::new(stdin());
        let mut buf = Vec::new();
        while reader.read_until(b'\0', &mut buf)? > 0 {
            self.storage.process(&buf);
            buf.clear();
        }
        Ok(())
    }

    /// Writes the environment to an [`OsString`].
    ///
    /// This concatenates the result of calling [`into_parts`](Environ::into_parts) on
    /// [`environ`](Self::environ).
    ///
    /// # Examples
    ///
    /// ```
    /// use viro::Viro;
    ///
    /// let mut env = Viro::new();
    /// env.insert("PATH", "/bin:/sbin:/usr/bin");
    /// env.insert("SHELL", "/bin/bash");
    ///
    /// let mut ser = env.write_string();
    /// assert_eq!(ser, "PATH=/bin:/sbin:/usr/bin\0");
    /// ```
    #[cfg_attr(feature = "nightly", coverage(off))]
    pub fn write_string(&mut self) -> OsString {
        self.environ().into_parts().collect()
    }

    /// Writes the environment to standard output.
    ///
    /// This outputs the result of calling [`into_parts`](Environ::into_parts) on
    /// [`environ`](Self::environ).
    ///
    /// # Examples
    ///
    /// ```no_run
    /// # fn main() -> std::io::Result<()> {
    /// use viro::Viro;
    ///
    /// let mut env = Viro::new();
    /// env.insert("PATH", "/bin:/sbin:/usr/bin");
    /// env.insert("SHELL", "/bin/bash");
    ///
    /// env.write_stdout()?;
    /// # Ok(())
    /// # }
    /// ```
    #[cfg_attr(feature = "nightly", coverage(off))]
    pub fn write_stdout(&mut self) -> io::Result<()> {
        let mut writer = BufWriter::new(stdout());
        self.environ()
            .into_parts()
            .try_for_each(|item| writer.write_all(item.as_encoded_bytes()))
    }
}
