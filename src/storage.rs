//! Stored environment variables.
use std::{
    array,
    borrow::Cow,
    ffi::{OsStr, OsString},
    fmt,
    iter::{Chain, FusedIterator},
    mem, slice, str,
};

use indexmap::{
    IndexMap, IndexSet,
    map::{self, RawEntryApiV1, raw_entry_v1::RawEntryMut},
    set::{self, Slice},
};

/// How a list is delimited.
#[derive(Copy, Clone)]
struct Config<S: ?Sized + AsRef<[u8]> = [u8]> {
    /// Whether the list allows a delimiter after all items.
    terminate: bool,

    /// Preferred delimiter for list.
    preferred_delim: u8,

    /// Allowed delimiters for list. Must necessarily include preferred delimiter.
    allowed_delims: S,
}
impl Config {
    /// Common code to verify that the preferred delimiter is included in the list of delimiters.
    const fn includes_delim(allowed_delims: &[u8], delim: u8) -> bool {
        let mut idx = 0;
        while idx < allowed_delims.len() {
            if allowed_delims[idx] == delim {
                return true;
            }
            idx += 1;
        }
        false
    }

    /// Common assertions for constructors.
    #[cfg_attr(feature = "nightly", coverage(off))]
    const fn assert_valid<const N: usize>(allowed_delims: [u8; N], preferred_delim: u8) {
        assert!(
            Self::includes_delim(&allowed_delims, preferred_delim),
            "preferred delim isn't included in allowed delims"
        );
        assert!(preferred_delim < 128, "preferred delim isn't ASCII",);
        let mut idx = 0;
        while idx < N {
            assert!(allowed_delims[idx] < 128, "allowed delim isn't ASCII",);
            idx += 1;
        }
    }

    /// List puts delimiter between items.
    #[cfg_attr(feature = "nightly", coverage(off))]
    const fn between<const N: usize>(
        allowed_delims: [u8; N],
        preferred_delim: u8,
    ) -> Config<[u8; N]> {
        Self::assert_valid(allowed_delims, preferred_delim);
        Config {
            terminate: false,
            allowed_delims,
            preferred_delim,
        }
    }

    /// List puts delimiter after items.
    #[cfg_attr(feature = "nightly", coverage(off))]
    const fn after<const N: usize>(
        allowed_delims: [u8; N],
        preferred_delim: u8,
    ) -> Config<[u8; N]> {
        Self::assert_valid(allowed_delims, preferred_delim);
        Config {
            terminate: true,
            allowed_delims,
            preferred_delim,
        }
    }

    /// Strips a trailing delimiter if needed.
    fn strip_delim<'a>(&self, mut s: &'a OsStr) -> &'a OsStr {
        if self.terminate {
            let has_last = s
                .as_encoded_bytes()
                .last()
                .is_some_and(|last| self.is_delim(*last));
            if has_last {
                // SAFETY: We verified that the last character is ASCII, because it's a delimiter.
                s = unsafe {
                    OsStr::from_encoded_bytes_unchecked(&s.as_encoded_bytes()[..s.len() - 1])
                };
            }
        }
        s
    }

    /// Checks if a byte is an allowed delimiter.
    const fn is_delim(&self, c: u8) -> bool {
        Self::includes_delim(&self.allowed_delims, c)
    }

    /// Gets the preferred delimiter as an [`OsStr`].
    fn delim(&self) -> &OsStr {
        // SAFETY: We only use ASCII characters for delimiters.
        OsStr::new(unsafe { str::from_utf8_unchecked(slice::from_ref(&self.preferred_delim)) })
    }

    /// Merges values with the delimiter.
    fn merge<'a>(&'static self, values: set::Iter<'a, OsString>) -> Merge<'a> {
        Merge {
            config: self,
            values,
            is_delim: false,
        }
    }

    /// Gets the configuration for a given environment variable.
    fn for_var(var: &OsStr) -> Option<&'static Config> {
        VECTOR_VARS.get(var.as_encoded_bytes()).copied()
    }
}
impl fmt::Debug for Config {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        struct Allowed<'a>(&'a [u8]);
        impl fmt::Debug for Allowed<'_> {
            fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                let mut list = f.debug_list();
                for b in self.0 {
                    list.entry(&format_args!("b{:?}", *b as char));
                }
                list.finish()
            }
        }
        f.debug_struct("Config")
            .field("terminate", &self.terminate)
            .field(
                "preferred_delim",
                &format_args!("b{:?}", self.preferred_delim as char),
            )
            .field("allowed_delims", &Allowed(self.allowed_delims.as_ref()))
            .finish()
    }
}

/// Delimited string for a vector, merged together.
#[derive(Clone)]
struct Merge<'a> {
    /// Configuration for vector.
    config: &'static Config,

    /// Values to merge.
    values: set::Iter<'a, OsString>,

    /// State of iterator.
    is_delim: bool,
}
/// Makes an empty version of this iterator.
impl<'a> Default for Merge<'a> {
    #[cfg_attr(feature = "nightly", coverage(off))]
    fn default() -> Merge<'a> {
        Merge {
            config: COLON_CONFIG,
            values: set::Iter::default(),
            is_delim: false,
        }
    }
}
impl<'a> Iterator for Merge<'a> {
    type Item = &'a OsStr;

    #[inline]
    fn next(&mut self) -> Option<Self::Item> {
        Some(if self.is_delim {
            self.is_delim = false;
            self.config.delim()
        } else {
            let item = self.values.next()?;
            self.is_delim = self.values.len() != 0 || self.config.terminate;
            item
        })
    }

    #[inline]
    fn size_hint(&self) -> (usize, Option<usize>) {
        let len = self.len();
        (len, Some(len))
    }

    #[inline]
    fn fold<B, F>(mut self, mut init: B, mut f: F) -> B
    where
        Self: Sized,
        F: FnMut(B, Self::Item) -> B,
    {
        let delim = self.config.delim();
        if self.is_delim {
            init = f(init, delim);
        }
        let Some(next) = self.values.next() else {
            return init;
        };
        init = f(init, next);
        for value in self.values {
            init = f(init, delim);
            init = f(init, value);
        }
        if self.config.terminate {
            init = f(init, delim);
        }
        init
    }
}
impl ExactSizeIterator for Merge<'_> {
    #[inline]
    fn len(&self) -> usize {
        let mut len = self.values.len() * 2;
        if len > 0 {
            len -= !self.config.terminate as usize;
        }
        len += self.is_delim as usize;
        len
    }
}
impl fmt::Debug for Merge<'_> {
    #[cfg_attr(feature = "nightly", coverage(off))]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_tuple("Merge")
            .field(&self.values)
            .field(&self.config)
            .finish()
    }
}

/// Stored vector.
#[derive(Clone)]
struct VectorStorage {
    /// Configuration for vector.
    config: &'static Config,

    /// Set of values for vector.
    values: IndexSet<OsString>,
}
impl VectorStorage {
    /// Creates a new stored vector.
    pub(crate) fn new(config: &'static Config) -> VectorStorage {
        VectorStorage {
            config,
            values: IndexSet::new(),
        }
    }

    /// Appends a single item to storage.
    fn append_one(&mut self, value: &OsStr, temp: &mut OsString) {
        value.clone_into(temp);
        *temp = self.values.replace(mem::take(temp)).unwrap_or_default();
    }

    /// Appends a delimited list of values to storage.
    pub(crate) fn append_delimited(&mut self, mut data: &OsStr) {
        let config = self.config;

        data = config.strip_delim(data);

        let mut temp = OsString::new();
        loop {
            if let Some(idx) = data
                .as_encoded_bytes()
                .iter()
                .position(|b| config.is_delim(*b))
            {
                let bytes = data.as_encoded_bytes();
                // SAFETY: We only split at ASCII characters.
                let (first, rest) = unsafe {
                    (
                        OsStr::from_encoded_bytes_unchecked(&bytes[..idx]),
                        OsStr::from_encoded_bytes_unchecked(&bytes[idx + 1..]),
                    )
                };
                data = rest;
                self.append_one(first, &mut temp);
            } else {
                self.append_one(data, &mut temp);
                break;
            }
        }
    }
}
impl fmt::Debug for VectorStorage {
    #[cfg_attr(feature = "nightly", coverage(off))]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_list().entries(&self.values).finish()
    }
}

/// Stored variable.
#[derive(Clone)]
enum VarStorage {
    /// Scalar variable.
    Scalar(OsString),

    /// Vector variable.
    Vector(VectorStorage),
}
impl VarStorage {
    /// Create storage for a variable.
    pub(crate) fn new(key: &OsStr) -> VarStorage {
        if let Some(config) = Config::for_var(key) {
            VarStorage::Vector(VectorStorage::new(config))
        } else {
            VarStorage::Scalar(OsString::new())
        }
    }

    /// Sets the storage to a value, either by replacing it or augmenting its list of items.
    pub(crate) fn set(&mut self, value: Cow<'_, OsStr>) {
        match self {
            VarStorage::Vector(vector) => {
                vector.append_delimited(&value);
            }
            VarStorage::Scalar(scalar) => match value {
                Cow::Borrowed(value) => value.clone_into(scalar),
                Cow::Owned(value) => scalar.clone_from(&value),
            },
        }
    }
}
impl fmt::Debug for VarStorage {
    #[cfg_attr(feature = "nightly", coverage(off))]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            VarStorage::Scalar(scalar) => fmt::Debug::fmt(scalar, f),
            VarStorage::Vector(vector) => fmt::Debug::fmt(vector, f),
        }
    }
}

/// Accessed scalar.
#[derive(Copy, Clone, PartialEq, Eq)]
pub struct Scalar<'a> {
    /// Key for scalar.
    key: &'a OsStr,

    /// Value for scalar.
    value: &'a OsStr,
}
impl<'a> Scalar<'a> {
    /// Converts the scalar into [`OsStr`] components that can be concatenated together.
    pub fn into_parts(self) -> ScalarParts<'a> {
        ScalarParts {
            inner: [self.key, OsStr::new("="), self.value].into_iter(),
        }
    }
}
impl fmt::Debug for Scalar<'_> {
    #[cfg_attr(feature = "nightly", coverage(off))]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}: {:?}", self.key, self.value)
    }
}
impl fmt::Display for Scalar<'_> {
    #[cfg_attr(feature = "nightly", coverage(off))]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}={}", self.key.display(), self.value.display())
    }
}

/// Iterator over the [`OsStr`] components of a [`Scalar`].
#[derive(Clone, Debug)]
pub struct ScalarParts<'a> {
    /// Inner iterator.
    inner: array::IntoIter<&'a OsStr, 3>,
}
/// Makes an empty version of this iterator.
impl<'a> Default for ScalarParts<'a> {
    #[inline]
    #[cfg_attr(feature = "nightly", coverage(off))]
    fn default() -> ScalarParts<'a> {
        let empty = OsStr::new("");
        let mut inner = [empty, empty, empty].into_iter();
        inner.by_ref().for_each(drop);
        ScalarParts { inner }
    }
}
impl<'a> Iterator for ScalarParts<'a> {
    type Item = &'a OsStr;
    #[inline]
    fn next(&mut self) -> Option<&'a OsStr> {
        self.inner.next()
    }
    #[inline]
    fn size_hint(&self) -> (usize, Option<usize>) {
        self.inner.size_hint()
    }
    #[inline]
    fn fold<B, F>(self, init: B, f: F) -> B
    where
        Self: Sized,
        F: FnMut(B, Self::Item) -> B,
    {
        self.inner.fold(init, f)
    }
}
impl FusedIterator for ScalarParts<'_> {}

/// Accessed vector.
#[derive(Copy, Clone)]
pub struct Vector<'a> {
    /// Config for vector
    config: &'static Config,

    /// Key for vector.
    key: &'a OsStr,

    /// Slice of values for vector.
    values: &'a Slice<OsString>,
}
impl<'a> Vector<'a> {
    /// Converts the vector into [`OsStr`] parts that can be concatenated.
    pub fn into_parts(self) -> VectorParts<'a> {
        VectorParts {
            inner: [self.key, OsStr::new("=")]
                .into_iter()
                .chain(self.config.merge(self.values.iter())),
        }
    }
}
impl fmt::Debug for Vector<'_> {
    #[cfg_attr(feature = "nightly", coverage(off))]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}: ", self.key)?;
        f.debug_list().entries(self.values).finish()
    }
}
impl fmt::Display for Vector<'_> {
    #[cfg_attr(feature = "nightly", coverage(off))]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}=", self.key.display())?;
        if self.config.terminate {
            for item in self.values {
                write!(f, "{}{}", item.display(), self.config.preferred_delim)?;
            }
        } else {
            let mut iter = self.values.iter();
            if let Some(item) = iter.next() {
                write!(f, "{}", item.display())?;
            }
            for item in self.values {
                write!(f, "{}{}", self.config.preferred_delim, item.display())?;
            }
        }
        Ok(())
    }
}

/// Iterator over the [`OsStr`] components of a [`Vector`].
#[derive(Clone, Debug)]
pub struct VectorParts<'a> {
    /// Inner iterator.
    inner: Chain<array::IntoIter<&'a OsStr, 2>, Merge<'a>>,
}
/// Makes an empty version of this iterator.
impl<'a> Default for VectorParts<'a> {
    #[inline]
    #[cfg_attr(feature = "nightly", coverage(off))]
    fn default() -> VectorParts<'a> {
        let empty = OsStr::new("");
        let mut iter = [empty, empty].into_iter();
        iter.by_ref().for_each(drop);
        VectorParts {
            inner: iter.chain(Merge::default()),
        }
    }
}
impl<'a> Iterator for VectorParts<'a> {
    type Item = &'a OsStr;
    #[inline]
    fn next(&mut self) -> Option<&'a OsStr> {
        self.inner.next()
    }
    #[inline]
    fn size_hint(&self) -> (usize, Option<usize>) {
        self.inner.size_hint()
    }
    #[inline]
    fn fold<B, F>(self, init: B, f: F) -> B
    where
        Self: Sized,
        F: FnMut(B, Self::Item) -> B,
    {
        self.inner.fold(init, f)
    }
}
impl FusedIterator for VectorParts<'_> {}

/// Accessed variable.
#[derive(Copy, Clone)]
pub enum Var<'a> {
    /// Scalar variable.
    Scalar(Scalar<'a>),

    /// Vector variable.
    Vector(Vector<'a>),
}
impl<'a> Var<'a> {
    /// Converts a storage tuple into a [`Var`].
    fn from_pair(key: &'a OsStr, value: &'a VarStorage) -> Var<'a> {
        match value {
            VarStorage::Scalar(value) => Var::Scalar(Scalar { key, value }),
            VarStorage::Vector(values) => Var::Vector(Vector {
                config: values.config,
                key,
                values: values.values.as_slice(),
            }),
        }
    }

    /// Converts the variable into [`OsStr`] parts that can be concatenated.
    #[cfg_attr(feature = "nightly", coverage(off))]
    pub fn into_parts(self) -> VarParts<'a> {
        match self {
            Var::Scalar(scalar) => VarParts::Scalar(scalar.into_parts()),
            Var::Vector(vector) => VarParts::Vector(vector.into_parts()),
        }
    }
}
impl fmt::Debug for Var<'_> {
    #[cfg_attr(feature = "nightly", coverage(off))]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Var::Scalar(scalar) => fmt::Debug::fmt(scalar, f),
            Var::Vector(vector) => fmt::Debug::fmt(vector, f),
        }
    }
}
impl fmt::Display for Var<'_> {
    #[cfg_attr(feature = "nightly", coverage(off))]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Var::Scalar(scalar) => fmt::Display::fmt(scalar, f),
            Var::Vector(vector) => fmt::Display::fmt(vector, f),
        }
    }
}

/// Iterator over the [`OsStr`] components of a [`Var`].
#[derive(Clone, Debug)]
pub enum VarParts<'a> {
    /// Scalar parts.
    Scalar(ScalarParts<'a>),

    /// Vector parts.
    Vector(VectorParts<'a>),
}
/// Makes an empty version of this iterator.
impl<'a> Default for VarParts<'a> {
    #[inline]
    #[cfg_attr(feature = "nightly", coverage(off))]
    fn default() -> VarParts<'a> {
        VarParts::Scalar(ScalarParts::default())
    }
}
impl<'a> Iterator for VarParts<'a> {
    type Item = &'a OsStr;
    #[inline]
    #[cfg_attr(feature = "nightly", coverage(off))]
    fn next(&mut self) -> Option<&'a OsStr> {
        match self {
            VarParts::Scalar(scalar) => scalar.next(),
            VarParts::Vector(vector) => vector.next(),
        }
    }
    #[inline]
    #[cfg_attr(feature = "nightly", coverage(off))]
    fn size_hint(&self) -> (usize, Option<usize>) {
        match self {
            VarParts::Scalar(scalar) => scalar.size_hint(),
            VarParts::Vector(vector) => vector.size_hint(),
        }
    }
    #[inline]
    #[cfg_attr(feature = "nightly", coverage(off))]
    fn fold<B, F>(self, init: B, f: F) -> B
    where
        Self: Sized,
        F: FnMut(B, Self::Item) -> B,
    {
        match self {
            VarParts::Scalar(scalar) => scalar.fold(init, f),
            VarParts::Vector(vector) => vector.fold(init, f),
        }
    }
}
impl FusedIterator for VarParts<'_> {}

/// Stored environment.
#[derive(Default, Clone)]
pub(crate) struct Storage {
    /// Inner map.
    inner: IndexMap<OsString, VarStorage>,
}
impl Storage {
    /// Makes empty environment.
    pub(crate) fn new() -> Storage {
        Storage {
            inner: IndexMap::new(),
        }
    }

    /// Inserts a key-value pair into storage.
    pub(crate) fn insert(&mut self, key: Cow<'_, OsStr>, value: Cow<'_, OsStr>) {
        if !EXCLUDED_VARS.contains(key.as_encoded_bytes()) {
            let entry = self.inner.raw_entry_mut_v1();
            match entry.from_key(&*key) {
                RawEntryMut::Occupied(entry) => {
                    entry.into_mut().set(value);
                }
                RawEntryMut::Vacant(entry) => {
                    let storage = VarStorage::new(&key);
                    entry.insert(key.into_owned(), storage).1.set(value);
                }
            }
        }
    }

    /// Removes a key from storage.
    pub(crate) fn remove(&mut self, key: &OsStr) {
        self.inner.shift_remove(key);
    }

    /// Gets a value from storage.
    pub(crate) fn get(&self, key: &OsStr) -> Option<Var<'_>> {
        let (key, value) = self.inner.get_key_value(key)?;
        Some(Var::from_pair(key, value))
    }

    /// Inserts an item into storage if it contains an equals sign, or removes it otherwise.
    ///
    /// Ignores empty items.
    pub(crate) fn process(&mut self, item: &[u8]) {
        let last_nonzero = item
            .iter()
            .rposition(|b| *b != b'\0')
            .map(|idx| idx + 1)
            .unwrap_or(0);
        let item = &item[..last_nonzero];
        if item.is_empty() {
            return;
        }
        if let Some(pos) = item.iter().position(|b| *b == b'=') {
            // SAFETY: We can always safely split on ASCII characters. (here, equals and NUL)
            let (key, value) = unsafe {
                (
                    OsStr::from_encoded_bytes_unchecked(&item[..pos]),
                    OsStr::from_encoded_bytes_unchecked(&item[pos + 1..]),
                )
            };
            self.insert(Cow::Borrowed(key), Cow::Borrowed(value));
        } else {
            // SAFETY: We can always safely split on ASCII characters. (here, NUL)
            let item = unsafe { OsStr::from_encoded_bytes_unchecked(item) };
            self.remove(item);
        }
    }

    /// Splits an item by NUL bytes, then processes each item.
    pub(crate) fn process_all(&mut self, data: &[u8]) {
        for item in data.split(|b| *b == b'\0') {
            self.process(item);
        }
    }

    /// Gets the full environment as an iterator.
    pub(crate) fn environ(&self) -> Environ<'_> {
        Environ {
            storage: self.inner.iter(),
        }
    }
}
impl fmt::Debug for Storage {
    #[cfg_attr(feature = "nightly", coverage(off))]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_map().entries(self.inner.iter()).finish()
    }
}

/// Iterator over all variables in the environment.
#[derive(Clone, Debug)]
pub struct Environ<'a> {
    /// Storage iterator.
    storage: map::Iter<'a, OsString, VarStorage>,
}
impl<'a> Environ<'a> {
    /// Converts the environment into [`OsStr`] parts that can be concatenated.
    pub fn into_parts(self) -> EnvironParts<'a> {
        EnvironParts {
            storage: self,
            var: VarParts::default(),
            is_delim: false,
        }
    }
}
impl<'a> Iterator for Environ<'a> {
    type Item = Var<'a>;

    #[inline]
    fn next(&mut self) -> Option<Var<'a>> {
        let (key, value) = self.storage.next()?;
        Some(Var::from_pair(key, value))
    }

    #[inline]
    fn size_hint(&self) -> (usize, Option<usize>) {
        self.storage.size_hint()
    }
}
impl<'a> DoubleEndedIterator for Environ<'a> {
    #[cfg_attr(feature = "nightly", coverage(off))]
    #[inline]
    fn next_back(&mut self) -> Option<Var<'a>> {
        let (key, value) = self.storage.next_back()?;
        Some(Var::from_pair(key, value))
    }
}
impl ExactSizeIterator for Environ<'_> {
    #[cfg_attr(feature = "nightly", coverage(off))]
    #[inline]
    fn len(&self) -> usize {
        self.storage.len()
    }
}
impl FusedIterator for Environ<'_> {}

/// Iterator over the [`OsStr`] components of all [`Var`]s.
#[derive(Clone, Debug)]
pub struct EnvironParts<'a> {
    /// Storage iterator.
    storage: Environ<'a>,

    /// Current variable iterator.
    var: VarParts<'a>,

    /// State of iterator.
    is_delim: bool,
}
impl<'a> Iterator for EnvironParts<'a> {
    type Item = &'a OsStr;
    #[inline]
    fn next(&mut self) -> Option<&'a OsStr> {
        loop {
            if let Some(var) = self.var.next() {
                return Some(var);
            } else if self.is_delim {
                self.is_delim = false;
                return Some(OsStr::new("\0"));
            } else {
                self.var = self.storage.next()?.into_parts();
                self.is_delim = true;
            }
        }
    }
    #[inline]
    fn size_hint(&self) -> (usize, Option<usize>) {
        let lo = self.storage.size_hint().0 * 3 + self.var.size_hint().0 + self.is_delim as usize;
        if lo == 0 { (0, Some(0)) } else { (lo, None) }
    }
    #[inline]
    fn fold<B, F>(self, mut init: B, mut f: F) -> B
    where
        Self: Sized,
        F: FnMut(B, Self::Item) -> B,
    {
        let delim = OsStr::new("\0");
        init = self.var.fold(init, &mut f);
        if self.is_delim {
            init = f(init, delim);
        }
        self.storage.fold(init, |mut init, var| {
            init = var.into_parts().fold(init, &mut f);
            f(init, delim)
        })
    }
}
impl FusedIterator for EnvironParts<'_> {}

/// Items separated by colons.
const COLON_CONFIG: &Config = &Config::between([b':'], b':');

/// Items separated by semicolons.
const SEMICOLON_CONFIG: &Config = &Config::between([b';'], b';');

/// Items separated by colons or semicolons.
const COLON_SEMICOLON_CONFIG: &Config = &Config::between([b':', b';'], b':');

/// Items terminated by colons.
const COLON_TERMINATE_CONFIG: &Config = &Config::after([b':'], b':');

/// Items separated by colons or spaces.
const COLON_SPACE_CONFIG: &Config = &Config::between([b':', b' '], b':');

/// Items separated by semicolons on Windows, and colons elsewhere.
const PATH_CONFIG: &Config = if cfg!(windows) {
    SEMICOLON_CONFIG
} else {
    COLON_CONFIG
};

/// Known environment variables that are outliers and must not be counted.
static EXCLUDED_VARS: phf::Set<&[u8]> = phf::phf_set! {
    b"_",
    b"GPG_TTY",
    b"HOME",
    b"NOTIFY_SOCKET",
    b"PWD",
    b"SHELL",
    b"SHLVL",
    b"USER",
};

/// All known environment variables that are lists to be merged.
static VECTOR_VARS: phf::Map<&[u8], &Config> = phf::phf_map! {
    b"GOPATH" => COLON_CONFIG,
    b"CAML_LD_LIBRARY_PATH" => COLON_SEMICOLON_CONFIG,
    b"LD_LIBRARY_PATH" => COLON_SEMICOLON_CONFIG,
    b"LD_PRELOAD" => COLON_SPACE_CONFIG,
    b"LS_COLORS" => COLON_TERMINATE_CONFIG,
    b"MANPATH" => COLON_CONFIG,
    b"PATH" => PATH_CONFIG,
    b"PERL5LIB" => COLON_CONFIG,
    b"PERLLIB" => COLON_CONFIG,
    b"PKG_CONFIG_PATH" => PATH_CONFIG,
    b"PYTHONPATH" => COLON_CONFIG,
    b"XDG_DATA_DIRS" => COLON_CONFIG,
};

#[cfg(test)]
mod tests {
    use std::{
        borrow::Cow,
        ffi::{OsStr, OsString},
        fmt,
    };

    use indexmap::{IndexSet, set::Slice};

    use crate::storage::{
        COLON_CONFIG, COLON_SPACE_CONFIG, COLON_TERMINATE_CONFIG, Config, Scalar, Storage, Vector,
        VectorStorage,
    };

    #[track_caller]
    #[cfg_attr(feature = "nightly", coverage(off))]
    fn test_parts<'a>(
        mut iter: impl Clone + Iterator<Item = &'a OsStr> + fmt::Debug,
        expected: &str,
    ) {
        let parts = iter
            .clone()
            .map(ToOwned::to_owned)
            .collect::<Vec<OsString>>();
        for idx in 0..parts.len() {
            assert!(
                iter.size_hint().0 <= parts.len() - idx,
                "lower_bound({:?}) > len({:?}) - {idx:?}",
                iter,
                parts
            );
            assert!(
                iter.size_hint().1.is_none_or(|x| x <= parts.len() - idx),
                "upper_bound({:?}) > len({:?}) - {idx:?}",
                iter,
                parts
            );
            let collected = iter.clone().collect::<OsString>();
            let folded = iter.clone().fold(OsString::new(), |mut lhs, rhs| {
                lhs.push(rhs);
                lhs
            });
            if idx == 0 {
                assert_eq!(collected, expected);
                assert_eq!(folded, expected);
            } else {
                assert_eq!(collected, folded);
            }
            iter.next();
        }
    }

    #[test]
    fn config_debug() {
        assert_eq!(
            format!("{COLON_SPACE_CONFIG:?}"),
            "Config { terminate: false, preferred_delim: b':', allowed_delims: [b':', b' '] }"
        );
    }

    #[test]
    fn scalar() {
        test_parts(
            Scalar {
                key: OsStr::new("key"),
                value: OsStr::new("value"),
            }
            .into_parts(),
            "key=value",
        );
    }

    #[test]
    fn vector_separated() {
        test_parts(
            Vector {
                config: COLON_CONFIG,
                key: OsStr::new("key"),
                values: IndexSet::from([OsString::from("val"), OsString::from("ue")]).as_slice(),
            }
            .into_parts(),
            "key=val:ue",
        );
        test_parts(
            Vector {
                config: COLON_CONFIG,
                key: OsStr::new("key"),
                values: Slice::new(),
            }
            .into_parts(),
            "key=",
        );
    }

    #[test]
    fn vector_terminated() {
        test_parts(
            Vector {
                config: COLON_TERMINATE_CONFIG,
                key: OsStr::new("key"),
                values: IndexSet::from([OsString::from("val"), OsString::from("ue")]).as_slice(),
            }
            .into_parts(),
            "key=val:ue:",
        );
        test_parts(
            Vector {
                config: COLON_TERMINATE_CONFIG,
                key: OsStr::new("key"),
                values: Slice::new(),
            }
            .into_parts(),
            "key=",
        );
    }

    #[test]
    fn vector_storage_append_one() {
        let mut storage = VectorStorage::new(Config::for_var(OsStr::new("PATH")).unwrap());
        let mut temp = OsString::new();
        storage.append_one(OsStr::new("/bin"), &mut temp);
        storage.append_one(OsStr::new("/sbin"), &mut temp);
        storage.append_one(OsStr::new("/bin"), &mut temp);
        storage.append_one(OsStr::new("/usr/bin"), &mut temp);
        assert_eq!(
            storage.values,
            IndexSet::from([
                OsString::from("/bin"),
                OsString::from("/sbin"),
                OsString::from("/usr/bin")
            ])
        );
    }

    #[test]
    fn vector_storage_append_separated() {
        let mut storage = VectorStorage::new(COLON_CONFIG);
        storage.append_delimited(OsStr::new("/bin:/sbin"));
        storage.append_delimited(OsStr::new("/sbin:/bin:/usr/bin"));
        assert_eq!(
            storage.values,
            IndexSet::from([
                OsString::from("/bin"),
                OsString::from("/sbin"),
                OsString::from("/usr/bin")
            ])
        );
    }

    #[test]
    fn vector_storage_append_terminated() {
        let mut storage = VectorStorage::new(COLON_TERMINATE_CONFIG);
        storage.append_delimited(OsStr::new("/bin:/sbin:"));
        storage.append_delimited(OsStr::new("/sbin:/bin:/usr/bin"));
        assert_eq!(
            storage.values,
            IndexSet::from([
                OsString::from("/bin"),
                OsString::from("/sbin"),
                OsString::from("/usr/bin")
            ])
        );
    }

    #[test]
    fn storage_basic() {
        let mut storage = Storage::new();
        storage.insert(
            Cow::Borrowed(OsStr::new("PATH")),
            Cow::Borrowed(OsStr::new("/bin:/sbin")),
        );
        storage.insert(
            Cow::Borrowed(OsStr::new("PATH")),
            Cow::Borrowed(OsStr::new("/sbin:/bin:/usr/bin")),
        );
        storage.insert(
            Cow::Borrowed(OsStr::new("SHELL")),
            Cow::Borrowed(OsStr::new("/bin/bash")),
        );
        storage.insert(
            Cow::Borrowed(OsStr::new("EDITOR")),
            Cow::Borrowed(OsStr::new("/usr/bin/kak")),
        );
        storage.insert(
            Cow::Borrowed(OsStr::new("BROWSER")),
            Cow::Owned(OsString::from("/usr/bin/firefox")),
        );
        assert_eq!(
            storage.inner.keys().collect::<Vec<_>>(),
            ["PATH", "EDITOR", "BROWSER"]
        );
        assert!(storage.get(OsStr::new("SHELL")).is_none());
        assert_eq!(
            storage
                .get(OsStr::new("PATH"))
                .unwrap()
                .into_parts()
                .collect::<OsString>(),
            "PATH=/bin:/sbin:/usr/bin",
        );
        assert_eq!(
            storage
                .get(OsStr::new("EDITOR"))
                .unwrap()
                .into_parts()
                .collect::<OsString>(),
            "EDITOR=/usr/bin/kak",
        );
        assert_eq!(
            storage
                .get(OsStr::new("BROWSER"))
                .unwrap()
                .into_parts()
                .collect::<OsString>(),
            "BROWSER=/usr/bin/firefox",
        );
        storage.remove(OsStr::new("PATH"));
        assert_eq!(
            storage.inner.keys().collect::<Vec<_>>(),
            ["EDITOR", "BROWSER"]
        );
    }

    #[test]
    fn storage_process() {
        let mut storage = Storage::new();
        storage.process_all(
            b"PATH=/bin:/sbin\0PATH=/sbin:/bin:/usr/bin\0SHELL=/bin/bash\0EDITOR=/usr/bin/kak\0BROWSER=/usr/bin/firefox\0"
        );
        test_parts(
            storage.environ().into_parts(),
            "PATH=/bin:/sbin:/usr/bin\0EDITOR=/usr/bin/kak\0BROWSER=/usr/bin/firefox\0",
        );
        storage.process_all(b"PATH");
        test_parts(
            storage.environ().into_parts(),
            "EDITOR=/usr/bin/kak\0BROWSER=/usr/bin/firefox\0",
        );
    }
}
