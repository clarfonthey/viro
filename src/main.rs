//! The viro program.
#![cfg_attr(feature = "nightly", feature(coverage_attribute))]
#![cfg_attr(feature = "nightly", coverage(off))]
use std::io;

use viro::Viro;

fn main() -> io::Result<()> {
    let mut viro = Viro::new();
    viro.read_environ();
    viro.read_stdin()?;
    viro.write_stdout()?;
    Ok(())
}
